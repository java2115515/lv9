package com.example.labvj009;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController implements SwitchScenes {
    @FXML
    private Circle circle;
    private double x;
    private double y;

    public void up() { circle.setCenterY(y=y-10); }
    public void down() { circle.setCenterY(y=y+10); }
    public void right() { circle.setCenterX(x=x+10); }
    public void left() { circle.setCenterX(x=x-10); }
    private Stage stage;
    private Scene scene;
    private Parent root;
    @FXML
    @Override
    public void switchToScene(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("secondScene.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


}