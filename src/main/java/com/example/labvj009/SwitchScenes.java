package com.example.labvj009;

import javafx.event.ActionEvent;

import java.io.IOException;

public interface SwitchScenes {
    void switchToScene(ActionEvent event) throws IOException;
}
